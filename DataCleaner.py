import pandas as pd
import gzip
import json
import re
from nltk.corpus import stopwords
import string
from nltk.stem import PorterStemmer

def parse(path):
  g = gzip.open(path, 'rb')
  for l in g:
    yield json.loads(l)

def getDF(data):
  i = 0
  df = {}
  for d in data:
    df[i] = d
    i += 1
  return pd.DataFrame.from_dict(df, orient='index')

def clean_texts(text_series):
  stemmer= PorterStemmer()
  words= stopwords.words("english")
  return text_series.apply(lambda x: " ".join([stemmer.stem(i)
                             for i in re.sub("[^a-zA-Z]", " ", str(x)).split()
                             if i not in words]).lower())
