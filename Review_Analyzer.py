import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
import DataCleaner as dc

reviews= dc.parse('Digital_Music_5.json.gz')
df = dc.getDF(reviews)
texts=[]
scores=[]
for index,row in df.iterrows():
  st= str(row['reviewText'])
  texts.append(st)
  scores.append(row['overall'])
vectorizer = CountVectorizer(token_pattern= r"(?u)\b\w+\b",
                             stop_words='english')
features_nd= vectorizer.fit_transform(texts)
#features= features_nd.toarray()
train_X,val_X,train_y,val_y= train_test_split(
        features_nd, 
        scores,
        train_size=0.80, 
        random_state=1234)
model= LogisticRegression()
model= model.fit(X=train_X, y=train_y)
prediction= model.predict(val_X)
print(accuracy_score(val_y,prediction))
