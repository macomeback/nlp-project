import DataCleaner as dc
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split
from sklearn.feature_selection import SelectKBest, chi2
from sklearn.pipeline import Pipeline
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import classification_report, confusion_matrix
import numpy as np
#import pickle
    
file_name= 'Magazine_Subscriptions_5.json.gz'
review= dc.parse(file_name)
df= dc.getDF(review)
df['cleaned']= dc.clean_texts(df['reviewText'])
vectorizer = TfidfVectorizer(min_df= 3, stop_words="english", sublinear_tf=True, norm='l2', ngram_range=(1, 2))
final_features = vectorizer.fit_transform(df['cleaned']).toarray()
X = df['cleaned']
Y = df['overall']
X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.25)
pipeline = Pipeline([('vect', vectorizer),
                     ('chi',  SelectKBest(chi2, k=1200)),
                     ('clf', RandomForestClassifier())])
# fitting our model and save it in a pickle for later use
model = pipeline.fit(X_train, y_train)
#with open('RandomForest.pickle', 'wb') as f:
    #pickle.dump(model, f)
ytest = np.array(y_test)
print(classification_report(ytest, model.predict(X_test)))
print(confusion_matrix(ytest, model.predict(X_test)))
#save_vocabs(wordas,file_name.split(".")[0]+'.txt')
